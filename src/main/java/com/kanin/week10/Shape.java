package com.kanin.week10;

public abstract class Shape {
    private String name;
    public Shape(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public double calArea(){
        return 0.0;
    }
    public double calPerimeter(){
        return 0.0;
    }
}
